const config = require("./../config/config.js");
const urlRegex = require('url-regex');

const c = require('irc-colors');
//coloring and formatting lib https://www.npmjs.com/package/irc-colors

const getVideoId = require('get-video-id');
//https://www.npmjs.com/package/get-video-id

const youtubeRegex = require('youtube-regex');
//https://www.npmjs.com/package/youtube-regex

const YouTube = require('youtube-node');
//https://github.com/nodenica/youtube-node

let youtubeClient = new YouTube();
youtubeClient.setKey(config.apiKeys.youtube);

function youtubeModule() {
    return function(client, raw_events, parsed_events) {
        parsed_events.use(youtube);
    };

    function youtube(command, event, client, next) {

        //Retrieve data from youtube url linked to channel
        if (command === 'privmsg' && youtubeRegex().test(event.message) === true) {

            //does some magic to trim string to length, .trim(length)
            String.prototype.trim = function(length) {
                return this.length > length ? this.substring(0, length) + "..." : this;
            };

            let urlsArray = [];

            event.message.match(urlRegex()).forEach(function(url) {
                urlsArray.push(url);
            });

            urlsArray.forEach(function(item) {
                let yId = getVideoId(item).id;

                youtubeClient.getById(yId, function(err, res) {
                    if (err) {
                        console.log(err);
                    }
                    else {
                        const result = { ref: res.items[0].snippet };

                        let yTitle = result.ref.title;
                        let yUploaded = result.ref.publishedAt;
                        let yChannel = result.ref.channelTitle;
                        let yDesc = result.ref.description.trim(140);

                        event.reply(c.bold("Title: ") + yTitle + c.red(' / ') + c.bold("Channel: ") + yChannel + c.red(' / ') + c.bold("Uploaded: ") + yUploaded + c.red(' / ') + c.bold("Description: ") + yDesc);

                        // console.log("res OBJECT: ", res);
                        // console.log("Title: " + yTitle + ' / ' + "Channel: " + yChannel + ' / ' + "Uploaded: " + yUploaded + ' / ' + "Description: " + yDesc);
                    }
                });
            });
        }

        //Youtube API search functionality with command !yt <search term>
        if (command === 'privmsg' && event.message.lastIndexOf('!yt', 0) === 0) { //if message starts with '!yt' and it is the first word in the message
            let messageArray = event.message.split(" ", 10);
            //shift removes the first element in the array > '!yt'
            messageArray.shift();

            // console.log("Message Array: ", messageArray);

            //join creates string from elements of array
            let searchTerm = messageArray.join(" ");

            // console.log("Search term: " + searchTerm);

            youtubeClient.search(searchTerm, 1, function(err, res) {
                if (err) {
                    console.log(err);
                }
                else {
                    // console.log(JSON.stringify(result, null, 2));

                    const result = { ref: res.items[0].snippet }
                    const yId = res.items[0].id.videoId;
                    const yURL = 'https://youtu.be/' + yId;

                    let yTitle = result.ref.title;
                    let yUploaded = result.ref.publishedAt;
                    let yChannel = result.ref.channelTitle;
                    let yDesc = result.ref.description.trim(140);

                    event.reply(c.bold("Title: ") + yTitle + c.red(' / ') + c.bold("Channel: ") + yChannel + c.red(' / ') + c.bold("Uploaded: ") + yUploaded + c.red(' / ') + c.bold("Description: ") + yDesc);
                    event.reply(c.bold("Watch Here: " + yURL));

                }
            });
        }
        next();
    }
}

module.exports = youtubeModule;
