const urlRegex = require('url-regex');
const youtubeRegex = require('youtube-regex');
const Xray = require("x-ray");
let xray = new Xray();

function urlTitleModule() {
    //some boilerplate for middleware functionality
    //middleware runs every time there is any kind of event (message, notice, join, part, quit)
    return function(client, raw_events, parsed_events) {
        parsed_events.use(urlTitle);
    };

    function urlTitle(command, event, client, next) {

        // console.log('[MyMiddleware]', command, event);

        // if message  contains url
        // privmsg = message in channel or query
        if (command === 'privmsg' && urlRegex().test(event.message) === true) {
            let capturedUrls = [];
            
            event.message.match(urlRegex()).forEach(function(url) {
                //for each url found in event.message
                //check https://www.npmjs.com/package/url-regex for usage
                // ^ .match(urlRegex()) is array of urls the message containts
                console.log(url);

                //if url is not youtube url 
                if (youtubeRegex().test(url) !== true) {
                    //push each url to capturedUrls array
                    capturedUrls.push(url);
                }
                else {
                    return false;
                }
            
            });

            //for each item in capturedUrls array
            capturedUrls.forEach(function(item) {
                //x-ray https://www.npmjs.com/package/x-ray
                //Scrapes given url (item) for title and prints it to channel if there is no error
                xray(item, 'title')(function(err, title) {
                    if (err) {
                        console.log(err);
                    }
                    else {
                        console.log("url: " + item + " / " + "title: " + title.trim());
                        //remove whitespaces inserted by some CMS around the title, .trim()
                        event.reply(title.trim());
                    }
                });
            });
        }
        next();
    }
}

module.exports = urlTitleModule;
