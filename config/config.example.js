
// Rename to config.js

var config = {
    channel: {
        name: '#channel',
        pw: 'password',
    },
    server: "irc.server.org",
    botName: "your bot name",
    realName: "purpose of your bot",
    apiKeys: {
        youtube: 'youtubeAPIkey'
    }
};

module.exports = config;