# mr robot IRC bot
### Features
* Retrieves website title from URL
* Retrieves Youtube information using API
  * Search with !yt search term
  * Retrieves information from youtube URL's


### Planned Features
- Weather Data using [Dark Sky API](https://darksky.net/dev/)
- Movie Data using [imdb-api package](https://www.npmjs.com/package/imdb-api)
  - OMDb API is now private, 1$ fee required for access [patreon](https://www.patreon.com/posts/api-is-going-10743518)

---

### How to use

* Update config file located in `config/config.js`
* run `npm start`

---

### Depends on
* [Kiwiirc/irc-framework](https://github.com/kiwiirc/irc-framework)

---
## Licence
* MIT

