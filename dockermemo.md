## Docker start up

### Create dockerfile
* Create file `nano Dockerfile`
  * Insert `FROM pipill/armhf-node` (pulls node image for ARMv7 (Raspberry Pi)
* Save the file

### Build docker image (once)
* Build dockerfile for armhf `docker image build -t mr_robot .`
 
### Run container
* Run container `docker run -it mr_robot:latest /bin/bash`

### Clone bot
* TODO: Write dockerfile to automate this procedure
  * Once inside image run `cd srv/ && git clone https://gitlab.com/ex1tium/mrRobotIRCbot.git && cd mrRobotIRCbot`
  * rename config file `mv config/config.example.js config/config.js`
  * edit with nano `nano-tiny config/config.js` (Install nano-tiny first from package if needed, instructions below)
  * Install dependencys `npm install`
  * Start bot `npm start`
 
### Detaching and attaching container

* Press `Ctrl+p` + `Ctrl+q` to detach from docker container
* To attach container again `docker attach mr_robot`

---

#### Useful packages, TODO automate in dockerfile
* Packages for basic operations inside `pipill/armhf-node` container
  * Nano package
      * nano editor for armhf  `wget http://ftp.fi.debian.org/debian/pool/main/n/nano/nano-tiny_2.2.6-1+b2_armhf.deb`
      * install `dpkg -i nano-tiny_2.2.6-1+b2_armhf.deb`
  * (OPTIONAL) Screen package 
      * screen for armhf: `wget http://ftp.fi.debian.org/debian/pool/main/s/screen/screen_4.1.0~20120320gitdb59704-7+deb7u1_armhf.deb`
      * install  `dpkg -i screen_4.1.0~20120320gitdb59704-7+deb7u1_armhf.deb`

#### Other docker commands
* remove stopped containers `docker container prune`
* list docker containers or images `docker image ls` `docker container ls`
* remove docker image `docker image rm mr_robot`
