//Import dependencys
const IRC = require("irc-framework");
const config = require("./config/config.js");

//Import modules
const urlTitleModule = require('./modules/urlTitle');
const youtubeModule = require("./modules/youtube");
const weatherModule = require("./modules/weather");

// Initialize IRC
const bot = new IRC.Client();

//Define what modules (Middleware) to enable
bot.use(youtubeModule());
bot.use(urlTitleModule());
bot.use(weatherModule());

//Bot settings, check ./config/config.js
bot.connect({
    host: config.server,
    port: 6667,
    nick: config.botName,
    gecos: config.realName,
    auto_reconnect: true,
	auto_reconnect_wait: 4000,
	auto_reconnect_max_retries: 3,
	ping_interval: 30,
	ping_timeout: 120
});

//Join channel after Bot has connected
bot.on('registered', function(){
    console.log("Connected to server");
    //JOIN THIS CHANNEL
    console.log(config.channels)
    bot.join(config.channel.name);
});

// Example functions
bot.on('close', function() {
	console.log('Connection close');
});

bot.on('userlist', function(event) {
	console.log('userlist for', event.channel, event.users);
});

bot.on('whois', function(event) {
	console.log(event);
});

bot.on('part', function(event) {
	console.log('user part', event);
});